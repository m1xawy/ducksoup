﻿using System;
using System.Collections.Generic;
using DuckSoup.Library;
using Newtonsoft.Json;

namespace DuckSoup.Gateway
{
    public class GatewaySettings : Settings
    {
        public int Version;
        public bool RemoveCaptcha;
        public string CaptchaCode;

        public List<RedirectionRule> RedirectionRules;
        
        public List<ushort> ClientWhitelist;
        public List<ushort> ClientBlacklist;

        public GatewaySettings Init()
        {
            Version = 1;
            RemoveCaptcha = true;
            CaptchaCode = "0";
            
            RedirectionRules = new List<RedirectionRule>()
            {
                new RedirectionRule()
                {
                    RemoteAddress = "0.0.0.0",
                    RemotePort = 1,
                    RedirectAddress = "1.1.1.1",
                    RedirectPort = 2
                }
            };
            
            ClientWhitelist = new List<ushort>()
            {
                0x2002, // CLIENT_GLOBAL_MODULE_KEEP_ALIVE (Empty)
                0x6100, // CLIENT_GATEWAY_PATCH_REQUEST 
                0x6104, // CLIENT_GATEWAY_NOTICE_REQUEST
                0x6101, // CLIENT_GATEWAY_SHARD_LIST_REQUEST 
                0x6106, // CLIENT_GATEWAY_SHARD_LIST_PING_REQUEST
                0x6102, // CLIENT_GATEWAY_LOGIN_REQUEST 
                0x6323 // CLIENT_GATEWAY_LOGIN_IBUV_CONFIRM_REQUEST
            };

            ClientBlacklist = new List<ushort>();
            
            return this;
        }
        
        public struct RedirectionRule
        {
            public string RemoteAddress;
            public int RemotePort;
            public string RedirectAddress;
            public int RedirectPort;
        }
    }
}