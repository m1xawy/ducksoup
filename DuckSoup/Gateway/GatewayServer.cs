using System;
using System.Linq;
using System.Threading.Tasks;
using DuckSoup.Library;
using SilkroadSecurityAPI;
#pragma warning disable 1998

namespace DuckSoup.Gateway
{
    public class GatewayServer : AsyncServer
    {
        public GatewayServer(SettingsManager settingsManager) : base(settingsManager)
        {
            PacketHandler = new PacketHandler(settingsManager.GatewaySettings.ClientWhitelist, settingsManager.GatewaySettings.ClientBlacklist);
            PacketHandler.RegisterModuleHandler(0xA102, SERVER_GATEWAY_LOGIN_RESPONSE);
            PacketHandler.RegisterModuleHandler(0x2322, SERVER_GATEWAY_LOGIN_IBUV_CHALLENGE);
            PacketHandler.RegisterModuleHandler(0xA104, SERVER_GATEWAY_NOTICE_RESPONSE);
            PacketHandler.RegisterModuleHandler(0xA100, SERVER_GATEWAY_PATCH_RESPONSE);
        }

        private async Task<PacketResult> SERVER_GATEWAY_PATCH_RESPONSE(Packet packet, Session session)
        {
            var overridePacket = new Packet(0xA100, packet.Encrypted, packet.Massive);
            var result = packet.ReadUInt8(); // 1	byte	result
            overridePacket.WriteUInt8(result);

            if(result == 0x02)
            {
                var errorCode = packet.ReadUInt8(); // 1	byte	errorCode
                overridePacket.WriteUInt8(errorCode);

                if(errorCode == 0x02)
                {
                    var bindPort = 0;
                    var bindAddress = "0.0.0.0";
                    
                    var downloadServerIp = packet.ReadAscii(); // *	string	DownloadServer.IP
                    var downloadServerPort = packet.ReadUInt16(); //2	ushort	DownloadServer.Port
                    var downloadServerCurVersion = packet.ReadUInt32(); //4	uint	DownloadServer.CurVersion

                    foreach (var redirectionRule in SettingsManager.GatewaySettings.RedirectionRules.Where(redirectionRule => redirectionRule.RemoteAddress == downloadServerIp && redirectionRule.RemotePort == downloadServerPort))
                    {
                        bindAddress = redirectionRule.RedirectAddress;
                        bindPort = redirectionRule.RedirectPort;
                    }
                    Logger.Debug(bindAddress);
                    Logger.Debug(bindPort);

                    overridePacket.WriteAscii(bindAddress);
                    overridePacket.WriteUInt16(bindPort);
                    overridePacket.WriteUInt32(downloadServerCurVersion);
                    
                    while(true)
                    {
                        var hasEntries = packet.ReadBool(); // 1	bool hasEntries
                        overridePacket.WriteBool(hasEntries);
                        if(!hasEntries)
                            break;

                        
                        overridePacket.WriteUInt32(packet.ReadUInt32()); //4	uint	file.ID
                        overridePacket.WriteAscii(packet.ReadAscii()); //    *	string	file.Name
                        overridePacket.WriteAscii(packet.ReadAscii()); //    *	string	file.Path
                        overridePacket.WriteUInt32(packet.ReadUInt32()); //4	uint	file.Length //in bytes
                        overridePacket.WriteBool(packet.ReadBool()); //1	bool	file.ToBePacked //into pk2
                    }		
                }
            }

            return new PacketResult(overridePacket, PacketResultType.Override);
        }
        
        private async Task<PacketResult> SERVER_GATEWAY_NOTICE_RESPONSE(Packet packet, Session session)
        {
            var dateTime = DateTime.Now;
            var noticePacket = new Packet(0xA104, false, true);
            noticePacket.WriteByte((byte) Program.DatabaseManager.Notice.Count);
            foreach (var notice in Program.DatabaseManager.Notice)
            {
                noticePacket.WriteAscii(notice.Subject);
                noticePacket.WriteAscii(notice.Article);
                noticePacket.WriteUInt16(notice.EditDate.Year);
                noticePacket.WriteUInt16(notice.EditDate.Month);
                noticePacket.WriteUInt16(notice.EditDate.Day);
                noticePacket.WriteUInt16(notice.EditDate.Hour);
                noticePacket.WriteUInt16(notice.EditDate.Minute);
                noticePacket.WriteUInt16(notice.EditDate.Second);
                noticePacket.WriteUInt32(notice.EditDate.Millisecond);
            }

            return new PacketResult(noticePacket, PacketResultType.Override);
        }

        private async Task<PacketResult> SERVER_GATEWAY_LOGIN_IBUV_CHALLENGE(Packet packet, Session session)
        {
            // if remove captcha is not enabled return nothing
            if (!SettingsManager.GatewaySettings.RemoveCaptcha) return new PacketResult(PacketResultType.Nothing);
            
            // create new packet, write it the captcha code, send it to the server and block the packet to the client
            var removePacket = new Packet(0x6323, false);
            removePacket.WriteAscii(SettingsManager.GatewaySettings.CaptchaCode);
            await session.SendToServer(removePacket);
            return new PacketResult(PacketResultType.Block);
        }

        private async Task<PacketResult> SERVER_GATEWAY_LOGIN_RESPONSE(Packet packet, Session session)
        {
            var flag = packet.ReadUInt8();
            if (flag != 0x01)
                return new PacketResult(PacketResultType.Block);

            var token = packet.ReadUInt32();
            var remoteAddress = packet.ReadAscii(); // host
            var remotePort = packet.ReadUInt16(); // host

            var bindPort = 0;
            var bindAddress = "0.0.0.0";

            foreach (var redirectionRule in SettingsManager.GatewaySettings.RedirectionRules.Where(redirectionRule => redirectionRule.RemoteAddress == remoteAddress && redirectionRule.RemotePort == remotePort))
            {
                bindAddress = redirectionRule.RedirectAddress;
                bindPort = redirectionRule.RedirectPort;
            }

            if(SettingsManager.GlobalSettings.DebugLevel >= DebugLevel.Debug)
                Logger.DebugFormat("{0} - Redirect - source {1}:{2} - target {3}:{4}",
                SettingsManager.GlobalSettings.ServerName, remoteAddress, remotePort, bindAddress, bindPort);

            // create a new Packet and override the old client before sending to client
            var redirectPacket = new Packet(0xA102, true);
            redirectPacket.WriteUInt8(0x01);
            redirectPacket.WriteUInt32(token);
            redirectPacket.WriteAscii(bindAddress);
            redirectPacket.WriteUInt16((ushort) bindPort);
            return new PacketResult(redirectPacket, PacketResultType.Override);
        }
    }
}