﻿// ReSharper disable IdentifierTypo
// ReSharper disable CollectionNeverUpdated.Global

// ReSharper disable UnusedMember.Global

namespace DuckSoup.Library
{
    public class Settings
    {
    }

    public class GlobalSettings : Settings
    {
        /**
        * None,
        * DownloadServer,
        * GatewayServer,
        * AgentServer,
        * ManagementServer
         */
        public int Version;

        public ServerType ServerType;
        public string ServerName;
        public string RemoteAddress;
        public string BindAddress;
        public int RemotePort;
        public int BindPort;
        public DatabaseSettings Database;
        public int ByteLimitation;

        /**
         * Nothing
         * All
         * PacketHandler
         * AllPackets
         * UnknownPackets
         */
        public DebugLevel DebugLevel;

        public GlobalSettings Init()
        {
            Version = 2;
            ServerType = ServerType.None;
            ServerName = "None-1";
            RemoteAddress = "0.0.0.0";
            BindAddress = "0.0.0.0";
            RemotePort = 1;
            BindPort = 1;
            ByteLimitation = 2048;
            DebugLevel = DebugLevel.Info;
            Database = new DatabaseSettings
            {
                Address = "0.0.0.0", Port = 1433, Username = "sa", Password = "123456", SharDb = "SRO_VT_SHARD",
                LogDb = "SRO_VT_LOG", AccountDb = "SRO_VT_ACCOUNT", ProxyDb = "DuckSoup"
            };

            return this;
        }

        public struct DatabaseSettings
        {
            public string Address;
            public int Port;
            public string Username;
            public string Password;
            public string SharDb;
            public string LogDb;
            public string AccountDb;
            public string ProxyDb;
        }
    }
}