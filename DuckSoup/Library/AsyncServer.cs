﻿using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using log4net;

namespace DuckSoup.Library
{
    public class AsyncServer
    {
        public PacketHandler PacketHandler;
        public readonly List<Session> Sessions = new List<Session>();
        protected readonly ILog Logger = Global.Logger;
        private TcpListener _tcpServer;
        public readonly SettingsManager SettingsManager;
        public IPEndPoint RemoteEndPoint;
        private bool _exit;
        public int Counter;

        protected AsyncServer(SettingsManager settingsManager)
        {
            SettingsManager = settingsManager;
        }

        private void AddSession(Session session)
        {
            Sessions.Add(session);
        }

        public void RemoveSession(Session session)
        {
            if(Sessions.Contains(session))
                Sessions.Remove(session);
        }

        public async Task Start()
        {
            if (SettingsManager.GlobalSettings.DebugLevel >= DebugLevel.Info)
            {
                Logger.InfoFormat("{0} - Debuglevel: {1}", SettingsManager.GlobalSettings.ServerName, SettingsManager.GlobalSettings.DebugLevel);
                Logger.InfoFormat("{0} - Servertype: {1}", SettingsManager.GlobalSettings.ServerName, SettingsManager.GlobalSettings.ServerType);
                Logger.InfoFormat("{0} - Setting up Socket..", SettingsManager.GlobalSettings.ServerName);
            }

            // defines a binding endpoint
            var bindEndPoint = new IPEndPoint(IPAddress.Parse(SettingsManager.GlobalSettings.BindAddress), SettingsManager.GlobalSettings.BindPort);
            RemoteEndPoint = new IPEndPoint(IPAddress.Parse(SettingsManager.GlobalSettings.RemoteAddress), SettingsManager.GlobalSettings.RemotePort);
            // starts the listener socket for the incoming connections
            _tcpServer = new TcpListener(bindEndPoint);
            _tcpServer.Start();
            
            if (SettingsManager.GlobalSettings.DebugLevel >= DebugLevel.Info)
            {
                Logger.InfoFormat("{0} - Server bound to {1}", SettingsManager.GlobalSettings.ServerName,
                    bindEndPoint);
                Logger.InfoFormat("{0} - Redirecting Sessions to {1}", SettingsManager.GlobalSettings.ServerName,
                    RemoteEndPoint);
            }

            // accepts client connections
            while(!_exit) {
                await _tcpServer.AcceptTcpClientAsync().ContinueWith(OnAccept, TaskScheduler.Default);
                await Task.Delay(3);
            }
        }
        
        public void Stop()
        {
            _exit = true;
            _tcpServer.Stop();
            Sessions.Clear();
        }

        private async Task OnAccept(Task<TcpClient> task)
        {
            if(SettingsManager.GlobalSettings.DebugLevel >= DebugLevel.Connections)
                Logger.InfoFormat("{0} - Got new TCP client from {1}", SettingsManager.GlobalSettings.ServerName, task.Result.Client.RemoteEndPoint as IPEndPoint);
            
            // creates a new session and starts it
            var clientSession = new Session(task.Result, this);
            AddSession(clientSession);
            await clientSession.Start();
            Counter++;
        }
    }
}