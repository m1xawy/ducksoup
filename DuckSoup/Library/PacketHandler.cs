using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using SilkroadSecurityAPI;
#pragma warning disable 1998

namespace DuckSoup.Library
{
    // ReSharper disable once InconsistentNaming
    public delegate Task<PacketResult> _PacketHandler(Packet packet, Session session);

    public readonly struct PacketResult
    {
        private readonly List<Packet> _packets;
        public readonly PacketResultType PacketResultType;
        
        public int PacketCount => PacketResultType == PacketResultType.Override ? _packets.Count : 0;

        public PacketResult(Packet packet, PacketResultType packetResultType = PacketResultType.Nothing)
        {
            _packets = null;
            if (packetResultType == PacketResultType.Override)
                _packets = new List<Packet> {packet};

            PacketResultType = packetResultType;
        }

        public PacketResult(PacketResultType packetResultType = PacketResultType.Nothing)
        {
            _packets = null;
            if (packetResultType == PacketResultType.Override)
                _packets = new List<Packet>();

            PacketResultType = packetResultType;
        }

        public Packet this[int index] => _packets[index];
    }

    public class PacketHandler
    {
        private readonly Hashtable _clientHandlers = new Hashtable();
        private readonly Hashtable _serverHandlers = new Hashtable();
        private readonly List<ushort> _clientWhitelist;
        private readonly List<ushort> _clientBlacklist;

        private _PacketHandler _defaultHandler;
        private _PacketHandler _blockHandler;
        private _PacketHandler _disconnectHandler;

        public PacketHandler(List<ushort> clientWhitelist, List<ushort> clientBlacklist)
        {
            SetDefaultHandler(HandleDefault);
            SetBlockHandler(HandleBlock);
            SetDisconnectHandler(HandleDisconnect);
            _clientWhitelist = clientWhitelist;
            _clientBlacklist = clientBlacklist;
        }

        private void SetDefaultHandler(_PacketHandler handler)
        {
            _defaultHandler = handler;
        }

        private async Task<PacketResult> HandleDefault(Packet packet, Session session)
        {
            return new PacketResult(PacketResultType.Nothing);
        }        
        
        private void SetBlockHandler(_PacketHandler handler)
        {
            _blockHandler = handler;
        }
        
        private async Task<PacketResult> HandleDisconnect(Packet packet, Session session)
        {
            return new PacketResult(PacketResultType.Disconnect);
        }
        
        private void SetDisconnectHandler(_PacketHandler handler)
        {
            _disconnectHandler = handler;
        }
        
        private async Task<PacketResult> HandleBlock(Packet packet, Session session)
        {
            return new PacketResult(PacketResultType.Block);
        }

        public void RegisterModuleHandler(ushort opcode, _PacketHandler handler)
        {
            _serverHandlers.Add(opcode, handler);
        }

        public void RegisterClientHandler(ushort opcode, _PacketHandler handler)
        {
            _clientHandlers.Add(opcode, handler);
        }

        public Task<PacketResult> HandleClient(Packet packet, Session session)
        {
            
            if (packet.Opcode == 0x9000 || packet.Opcode == 0x5000 || packet.Opcode == 0x2001)
                return _defaultHandler(packet, session);
                        
            // automatically blocks all packets that are not on the whitelist!
            if (_clientBlacklist.Contains(packet.Opcode))
                return _disconnectHandler(packet, session);
            
            if (!_clientWhitelist.Contains(packet.Opcode))
                return _blockHandler(packet, session);

            var handler = _clientHandlers[packet.Opcode] as _PacketHandler;
            return handler?.Invoke(packet, session) ?? _defaultHandler(packet, session);
        }

        public Task<PacketResult> HandleServer(Packet packet, Session session)
        {
            var handler = _serverHandlers[packet.Opcode] as _PacketHandler;
            return handler?.Invoke(packet, session) ?? _defaultHandler(packet, session);
        }
    }
}