﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using SilkroadSecurityAPI;

namespace DuckSoup.Library
{
    public sealed class Session
    {
        private readonly byte[] _clientBuffer = new byte[4096];
        private readonly byte[] _serverBuffer = new byte[4096];
        private readonly Security _serverSecurity = new Security();
        private readonly Security _clientSecurity = new Security();
        private TcpClient _clientTcpClient;
        private TcpClient _serverTcpClient;

        private readonly ILog _logger = Global.Logger;
        private readonly AsyncServer _asyncServer;

        public readonly int ClientId;
        public readonly string ClientIp;
        private bool _exit;

        #region Features

        public string Hwid;
        public string Charname;
        public uint UniqueCharId;
        public bool CharacterGameReady = false;
        public int LatestRegionId;
        public int SectorX;
        public int SectorY;
        public int PositionX;
        public int PositionY;
        public int PositionZ;

        #endregion

        #region Protection

        // Packet Modification
        public int PacketLength;

        // Packet Flooding
        private int _packetSize;
        private DateTime _lastPacketReset;

        // False Packets
        public bool CharnameSent = false;
        public bool CharScreen = false;
        public bool UserLoggedIn = false;
        public int Charid;

        #endregion


        public Session(TcpClient clientTcpClient, AsyncServer asyncServer)
        {
            _asyncServer = asyncServer;
            _clientTcpClient = clientTcpClient;

            if (_asyncServer.SettingsManager.GlobalSettings.DebugLevel >= DebugLevel.Connections)
                _logger.InfoFormat("{0} - Preparing Session..", asyncServer.SettingsManager.GlobalSettings.ServerName);


            // generates a "unique" id from the address and port hashcode and safes ip
            if (!(_clientTcpClient.Client.RemoteEndPoint is IPEndPoint ep)) return;
            ClientId = ep.Address.GetHashCode() + ep.Port.GetHashCode();
            ClientIp = ep.Address.ToString();
        }

        public async Task SendToClient(Packet packet)
        {
            if (_clientTcpClient == null || _serverTcpClient == null)
                return;

            try
            {
                _clientSecurity.Send(packet);
                await TransferToClient();
            }
            catch (Exception)
            {
                await Destroy();
            }
        }

        public async Task SendToServer(Packet packet)
        {
            if (_clientTcpClient == null || _serverTcpClient == null)
                return;

            try
            {
                _serverSecurity.Send(packet);
                await TransferToServer();
            }
            catch (Exception)
            {
                await Destroy();
            }
        }

        public async Task SendNotice(string message)
        {
            var notice = new Packet(0x3026, false, false);
            notice.WriteByte(7);
            notice.WriteAscii(message);
            await SendToClient(notice);
        }

        public async Task Start()
        {
            if (_asyncServer.SettingsManager.GlobalSettings.DebugLevel >= DebugLevel.Connections)
                _logger.InfoFormat("{0} - Starting Session..", _asyncServer.SettingsManager.GlobalSettings.ServerName);

            _clientSecurity.GenerateSecurity(true, true, true);
            // creates a new server socket and connects it according to the remote addr. and port 
            _serverTcpClient = new TcpClient();
            await _serverTcpClient.ConnectAsync(_asyncServer.RemoteEndPoint.Address, _asyncServer.RemoteEndPoint.Port);

            // starts receiving loop from server + client - if a destroy was called the loop brakes
            _ = Task.Factory.StartNew(async () =>
            {
                while (!_exit)
                {
                    await DoReceiveFromServer();
                }
            }, CancellationToken.None, TaskCreationOptions.LongRunning, TaskScheduler.Default);

            _ = Task.Factory.StartNew(async () =>
            {
                while (!_exit)
                {
                    await DoReceiveFromClient();
                }
            }, CancellationToken.None, TaskCreationOptions.LongRunning, TaskScheduler.Default);
        }

        public async Task Stop()
        {
            if (_asyncServer.SettingsManager.GlobalSettings.DebugLevel >= DebugLevel.Connections)
                _logger.InfoFormat("{0} - Stop Session - {1} ({2})",
                    _asyncServer.SettingsManager.GlobalSettings.ServerName, ClientId,
                    ClientIp);
            await Destroy();
        }

        private async Task DoReceiveFromClient()
        {
            try
            {
                // receive stuff
                var recvCount = await _clientTcpClient.GetStream().ReadAsync(_clientBuffer, 0, _clientBuffer.Length);

                if (recvCount == 0)
                {
                    await Destroy();
                    return;
                }

                // starts receiving again
                _clientSecurity.Recv(_clientBuffer, 0, recvCount);

                // transfers the incoming packets to a list
                var receivedPackets = _clientSecurity.TransferIncoming();


                // if there are no packets start receiving again
                if (receivedPackets == null) goto transfer;

                // loop through all received packets
                foreach (var packet in receivedPackets)
                {
                    // ignore handshake
                    if (packet.Opcode == 0x9000 || packet.Opcode == 0x5000 || packet.Opcode == 0x2001)
                        continue;

                    #region Protection

                    // Packet Modifying
                    PacketLength = packet.GetBytes().Length;

                    // Packet Flooding
                    // calc the last checktime
                    var lastCheckDiff = (DateTime.Now - _lastPacketReset).TotalSeconds;
                    // lastcheckdiff * bytelimitation for exact bytes per time - don't need to round it to one second
                    var maxBytesPerTime = lastCheckDiff * _asyncServer.SettingsManager.GlobalSettings.ByteLimitation;
                    // if the packetsize exceeded the calculated value and it was measured over 2 second (to prevent super random lag dcs)
                    if (_packetSize > maxBytesPerTime && lastCheckDiff > 2.0)
                    {
                        if (_asyncServer.SettingsManager.GlobalSettings.DebugLevel >= DebugLevel.Warning)
                            _logger.WarnFormat(
                                "{0} - Client {1}({2}) exceedet the byte limit: {3} (maximum: {4} - Last check {5} seconds ago)",
                                _asyncServer.SettingsManager.GlobalSettings.ServerName, ClientId, ClientIp,
                                _packetSize, maxBytesPerTime, lastCheckDiff);
                        await Destroy();
                    }
                    else if (lastCheckDiff > 1)
                    {
                        // else reset
                        _lastPacketReset = DateTime.Now;
                        _packetSize = 0;
                    }

                    #endregion

                    var packetResult = await _asyncServer.PacketHandler.HandleClient(packet, this);

                    // debug
                    if (_asyncServer.SettingsManager.GlobalSettings.DebugLevel >= DebugLevel.Debug)
                        _logger.DebugFormat("{0} - DoRecvFromClient Packet: 0x{1:X} - {2} ({3}) - Status: {4} ",
                            _asyncServer.SettingsManager.GlobalSettings.ServerName, packet.Opcode, ClientId, ClientIp,
                            packetResult.PacketResultType);

                    switch (packetResult.PacketResultType)
                    {
                        case PacketResultType.Override:
                            for (var index = 0; index < packetResult.PacketCount; index++)
                            {
                                _serverSecurity.Send(packetResult[index]);
                            }

                            break;
                        case PacketResultType.Block:
                            break;
                        case PacketResultType.Disconnect:
                            await Destroy();
                            break;
                        case PacketResultType.Nothing:
                            _serverSecurity.Send(packet);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                // transfers packets to the server and starts receiving from client again
                transfer:
                {
                    await TransferToServer();
                }
            }
            catch (Exception)
            {
                await Destroy();
            }
        }

        private async Task DoReceiveFromServer()
        {
            try
            {
                // receive stuff
                var recvCount = await _serverTcpClient.GetStream().ReadAsync(_serverBuffer, 0, _serverBuffer.Length);

                if (recvCount == 0)
                {
                    await Destroy();
                    return;
                }

                // starts receiving again
                _serverSecurity.Recv(_serverBuffer, 0, recvCount);

                // transfers the incoming packets to a list
                var receivedPackets = _serverSecurity.TransferIncoming();

                // if there are no packets start receiving again
                if (receivedPackets == null) goto transfer;

                // loop through all received packets
                foreach (var packet in receivedPackets)
                {
                    // ignore handshake
                    if (packet.Opcode == 0x5000 || packet.Opcode == 0x9000)
                        continue;

                    // debug
                    if (_asyncServer.SettingsManager.GlobalSettings.DebugLevel >= DebugLevel.Debug)
                        Global.Logger.DebugFormat("{0} - DoRecvFromServer Packet: 0x{1:X} - {2} ({3})",
                            _asyncServer.SettingsManager.GlobalSettings.ServerName, packet.Opcode, ClientId, ClientIp);

                    var packetResult = await _asyncServer.PacketHandler.HandleServer(packet, this);

                    switch (packetResult.PacketResultType)
                    {
                        case PacketResultType.Override:
                            for (var index = 0; index < packetResult.PacketCount; index++)
                            {
                                _clientSecurity.Send(packetResult[index]);
                            }

                            break;
                        case PacketResultType.Block:
                            break;
                        case PacketResultType.Disconnect:
                            await Destroy();
                            break;
                        case PacketResultType.Nothing:
                            _clientSecurity.Send(packet);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                // transfers packets to the client and starts receiving from server again
                transfer:
                {
                    await TransferToClient();
                }
            }
            catch (Exception)
            {
                await Destroy();
            }
        }

        private async Task TransferToClient()
        {
            try
            {
                var kvp = _clientSecurity.TransferOutgoing();

                if (kvp == null) return;

                foreach (var t in kvp)
                {
                    //transfers the client packets to the client
                    await _clientTcpClient.GetStream().WriteAsync(t.Key.Buffer, 0, t.Key.Buffer.Length);
                }
            }
            catch (Exception)
            {
                await Destroy();
            }
        }

        private async Task TransferToServer()
        {
            try
            {
                var kvp = _serverSecurity.TransferOutgoing();
                if (kvp == null) return;
                foreach (var t in kvp)
                {
                    _packetSize += t.Key.Buffer.Length;
                    //transfers the server packets to the server
                    await _serverTcpClient.GetStream().WriteAsync(t.Key.Buffer, 0, t.Key.Buffer.Length);
                }
            }
            catch (Exception)
            {
                await Destroy();
            }
        }

        private async Task Destroy()
        {
            await Task.Run(() =>
            {
                if (_exit)
                    return;

                if (_clientTcpClient != null)
                    if (_asyncServer.SettingsManager.GlobalSettings.DebugLevel >= DebugLevel.Connections)
                        _logger.InfoFormat("{0} - Lost TCP client connection from {1}",
                            _asyncServer.SettingsManager.GlobalSettings.ServerName,
                            _clientTcpClient.Client.RemoteEndPoint);

                // double socket close prevention
                _exit = true;
                // closes sockets and nulls them

                _clientTcpClient?.Close();
                _serverTcpClient?.Close();

                _clientTcpClient = null;
                _serverTcpClient = null;

                // removes the session from the session list - the function has a contains check
                _asyncServer.RemoveSession(this);
            });
        }
    }
}