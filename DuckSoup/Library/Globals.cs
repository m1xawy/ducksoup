﻿namespace DuckSoup.Library
{
    public static class Global
    {
        public static readonly log4net.ILog Logger =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod()?.DeclaringType);
        
    }
    
    public enum PacketResultType
    {
        Disconnect,
        Override,
        Block,
        Nothing
    }
    
    public enum ServerType
    {
        None,
        DownloadServer,
        GatewayServer,
        AgentServer,
        ManagementServer
    }
    
    public enum DebugLevel
    {
        Nothing, // do not use!
        Fatal, // failure but the server can't keep going
        Critical, // failure but the server can keep going
        Warning, // exploits, protection stuff
        Info, // everything that regards the server
        Connections, // Connection stuff (incoming / outgoing, destroying and so on)
        Debug, // debugging of Packets
        Everything
    }
}