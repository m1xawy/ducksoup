﻿using System.IO;
using DuckSoup.Agent;
using DuckSoup.Download;
using DuckSoup.Gateway;
using DuckSoup.Management;
using log4net;
using Newtonsoft.Json;

namespace DuckSoup.Library
{
    public class SettingsManager
    {
        public readonly GlobalSettings GlobalSettings;
        public readonly GatewaySettings GatewaySettings;
        public readonly DownloadSettings DownloadSettings;
        public readonly AgentSettings AgentSettings;
        public readonly ManagementSettings ManagementSettings;
        private readonly ILog _logger = Global.Logger;

        public SettingsManager()
        {
            if (!File.Exists(@"./GlobalSettings.json"))
            {
                GlobalSettings = new GlobalSettings().Init();
                File.WriteAllText(@"./GlobalSettings.json", JsonConvert.SerializeObject(GlobalSettings, Formatting.Indented));
                _logger.WarnFormat("GlobalSettings.json created. Please stop the Proxy and configure the file.");
            }
            
            if (!File.Exists(@"./GatewaySettings.json"))
            {
                GatewaySettings = new GatewaySettings().Init();
                File.WriteAllText(@"./GatewaySettings.json", JsonConvert.SerializeObject(GatewaySettings, Formatting.Indented));
                _logger.WarnFormat("GatewaySettings.json created. Please stop the Proxy and configure the file.");
            }
            
            if (!File.Exists(@"./DownloadSettings.json"))
            {
                DownloadSettings = new DownloadSettings().Init();
                File.WriteAllText(@"./DownloadSettings.json", JsonConvert.SerializeObject(DownloadSettings, Formatting.Indented));
                _logger.WarnFormat("DownloadSettings.json created. Please stop the Proxy and configure the file.");
            }
            
            if (!File.Exists(@"./AgentSettings.json"))
            {
                AgentSettings = new AgentSettings().Init();
                File.WriteAllText(@"./AgentSettings.json", JsonConvert.SerializeObject(AgentSettings, Formatting.Indented));
                _logger.WarnFormat("AgentSettings.json created. Please stop the Proxy and configure the file.");
            }
            
            if (!File.Exists(@"./ManagementSettings.json"))
            {
                ManagementSettings = new ManagementSettings().Init();
                File.WriteAllText(@"./ManagementSettings.json", JsonConvert.SerializeObject(ManagementSettings, Formatting.Indented));
                _logger.WarnFormat("ManagementSettings.json created. Please stop the Server and configure the file.");
            }


            // if a config file exists deserialize and initialize it
            GlobalSettings = JsonConvert.DeserializeObject<GlobalSettings>(File.ReadAllText(@"./GlobalSettings.json"));
            GatewaySettings = JsonConvert.DeserializeObject<GatewaySettings>(File.ReadAllText(@"./GatewaySettings.json"));
            DownloadSettings = JsonConvert.DeserializeObject<DownloadSettings>(File.ReadAllText(@"./DownloadSettings.json"));
            AgentSettings = JsonConvert.DeserializeObject<AgentSettings>(File.ReadAllText(@"./AgentSettings.json"));
            ManagementSettings = JsonConvert.DeserializeObject<ManagementSettings>(File.ReadAllText(@"./ManagementSettings.json"));
            
            if(GlobalSettings.Version < new GlobalSettings().Init().Version)
                _logger.WarnFormat("Settings.json is a older Version. (Current Version: {0} - Your Version: {1}) It might be that the Proxies won't work proberly. Please delete and reconfigure the file.", new GlobalSettings().Init().Version, GlobalSettings.Version);
            
            if(GatewaySettings.Version < new GatewaySettings().Init().Version)
                _logger.WarnFormat("GatewaySettings.json is a older Version. (Current Version: {0} - Your Version: {1}) It might be that the Gatewayproxy won't work proberly. Please delete and reconfigure the file.", new GatewaySettings().Init().Version, GatewaySettings.Version);
            
            if(DownloadSettings.Version < new DownloadSettings().Init().Version)
                _logger.WarnFormat("DownloadSettings.json is a older Version. (Current Version: {0} - Your Version: {1}) It might be that the Downloadproxy won't work proberly. Please delete and reconfigure the file.", new DownloadSettings().Init().Version, DownloadSettings.Version);
            
            if(AgentSettings.Version < new AgentSettings().Init().Version)
                _logger.WarnFormat("AgentSettings.json is a older Version. (Current Version: {0} - Your Version: {1}) It might be that the Agentproxy won't work proberly. Please delete and reconfigure the file.", new AgentSettings().Init().Version, AgentSettings.Version);
            
            if(ManagementSettings.Version < new ManagementSettings().Init().Version)
                _logger.WarnFormat("ManagementSettings.json is a older Version. (Current Version: {0} - Your Version: {1}) It might be that the ManagementServer won't work proberly. Please delete and reconfigure the file.", new ManagementSettings().Init().Version, ManagementSettings.Version);
        }
        
    }
}