﻿namespace DuckSoup.Library.Database.Models
{
    public class RefObjCommon
    {
        public int Service { get; set; }
        public int ID{ get; set; } // auto increment
        public string CodeName128{ get; set; }
        public string ObjName128{ get; set; }
        public string OrgObjCodeName128{ get; set; }
        public string DescStrId128{ get; set; }
        public int CashItem{ get; set; }
        public int Bionic{ get; set; }
        public int TypeID1{ get; set; }
        public int TypeID2{ get; set; }
        public int TypeID3{ get; set; }
        public int TypeID4{ get; set; }
        public int DecayTime{ get; set; }
        public int Country{ get; set; }
        public int Rarity{ get; set; }
        public int CanTrade{ get; set; }
        public int CanSell{ get; set; }
        public int CanBuy{ get; set; }
        public int CanBorrow{ get; set; }
        public int CanDrop{ get; set; }
        public int CanPick{ get; set; }
        public int CanRepair{ get; set; }
        public int CanRevive{ get; set; }
        public int CanUse{ get; set; }
        public int CanThrow{ get; set; }
        public int Price{ get; set; }
        public int CostRepair{ get; set; }
        public int CostRevive{ get; set; }
        public int CostBorrow{ get; set; }
        public int KeepingFee{ get; set; }
        public int SellPrice{ get; set; }
        public int ReqLevelType1{ get; set; }
        public int ReqLevel1{ get; set; }
        public int ReqLevelType2{ get; set; }
        public int ReqLevel2{ get; set; }
        public int ReqLevelType3{ get; set; }
        public int ReqLevel3{ get; set; }
        public int ReqLevelType4{ get; set; }
        public int ReqLevel4{ get; set; }
        public int MaxContain{ get; set; }
        public int RegionID{ get; set; }
        public int Dir{ get; set; }
        public int OffsetX{ get; set; }
        public int OffsetY{ get; set; }
        public int OffsetZ{ get; set; }
        public int Speed1{ get; set; }
        public int Speed2{ get; set; }
        public int Scale{ get; set; }
        public int BCHeight{ get; set; }
        public int BCRadius{ get; set; }
        public int EventID{ get; set; }
        public string AssocFileObj128{ get; set; }
        public string AssocFileDrop128{ get; set; }
        public string AssocFileIcon128{ get; set; }
        public string AssocFile1_128{ get; set; }
        public string AssocFile2_128{ get; set; }
        public int Link{ get; set; }
    }
}