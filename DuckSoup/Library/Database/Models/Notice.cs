﻿using System;

namespace DuckSoup.Library.Database.Models
{
    public class Notice
    {
        public int ID { get; set; }
        public int ContentID { get; set; }
        public string Subject { get; set; }
        public string Article { get; set; }
        public DateTime EditDate { get; set; }
    }
}