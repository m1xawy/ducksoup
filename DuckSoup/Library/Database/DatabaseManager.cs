﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DuckSoup.Library.Database.Models;
using log4net;

#pragma warning disable 1998

namespace DuckSoup.Library.Database
{
    public class DatabaseManager
    {
        // Docs: https://github.com/tmenier/AsyncPoco
        private SettingsManager _settingsManager;
        private ILog _logger = Global.Logger;
        private string ConnectionString { get; }
        public IDbConnection connection;


        public List<RefObjCommon> RefObjCommon;
        public List<RefSkill> RefSkill;
        public List<Notice> Notice;

        public DatabaseManager(SettingsManager settingsManager)
        {
            _settingsManager = settingsManager;
            var dbSettings = settingsManager.GlobalSettings.Database;
            ConnectionString =
                $"Server={dbSettings.Address},{dbSettings.Port};Initial Catalog=MASTER;User Id={dbSettings.Username};Password={dbSettings.Password};MultipleActiveResultSets=True;";
            connection = new SqlConnection(ConnectionString);


            try
            {
                connection.Query("Select GetDate() as CurrentDate");
                if (_settingsManager.GlobalSettings.DebugLevel >= DebugLevel.Info)
                    _logger.InfoFormat("{0} - Database connected!", _settingsManager.GlobalSettings.ServerName);

                switch (settingsManager.GlobalSettings.ServerType)
                {
                    case ServerType.AgentServer:
                        var loadItems = LoadItems();
                        var loadSkills = LoadSkills();
                        break;
                    case ServerType.GatewayServer:
                        var loadNotices = LoadNotices();
                        break;
                    case ServerType.DownloadServer:
                        break;
                    case ServerType.None:
                        break;
                    case ServerType.ManagementServer:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            catch (SqlException exception)
            {
                if (_settingsManager.GlobalSettings.DebugLevel >= DebugLevel.Critical)
                    _logger.InfoFormat("{0} - Could not establish Database connection! {1}",
                        _settingsManager.GlobalSettings.ServerName, exception.Message);
                
                Program.Stop();
            }
        }

        public async Task LoadItems()
        {
            if (_settingsManager.GlobalSettings.DebugLevel >= DebugLevel.Info)
                _logger.InfoFormat("{0} - Starting to read RefObjCommon. This might take a while!",
                    _settingsManager.GlobalSettings.ServerName);

            RefObjCommon = connection
                .QueryAsync<RefObjCommon>(
                    $"SELECT * FROM {_settingsManager.GlobalSettings.Database.SharDb}.dbo._RefObjCommon").Result
                .ToList();

            if (_settingsManager.GlobalSettings.DebugLevel >= DebugLevel.Info)
                _logger.InfoFormat("{0} - {1} Items loaded.", _settingsManager.GlobalSettings.ServerName,
                    RefObjCommon.Count());
        }

        public async Task LoadSkills()
        {
            if (_settingsManager.GlobalSettings.DebugLevel >= DebugLevel.Info)
                _logger.InfoFormat("{0} - Starting to read RefSkill. This might take a while!",
                    _settingsManager.GlobalSettings.ServerName);

            RefSkill = connection
                .QueryAsync<RefSkill>($"SELECT * FROM {_settingsManager.GlobalSettings.Database.SharDb}.dbo._RefSkill")
                .Result.ToList();

            if (_settingsManager.GlobalSettings.DebugLevel >= DebugLevel.Info)
                _logger.InfoFormat("{0} - {1} Skills loaded.", _settingsManager.GlobalSettings.ServerName,
                    RefSkill.Count());
        }

        public async Task LoadNotices()
        {
            if (_settingsManager.GlobalSettings.DebugLevel >= DebugLevel.Info)
                _logger.InfoFormat("{0} - Starting to read Notice. This might take a while!",
                    _settingsManager.GlobalSettings.ServerName);

            Notice = connection
                .QueryAsync<Notice>($"SELECT * FROM {_settingsManager.GlobalSettings.Database.AccountDb}.dbo._Notice")
                .Result.ToList();

            if (_settingsManager.GlobalSettings.DebugLevel >= DebugLevel.Info)
                _logger.InfoFormat("{0} - {1} Notices loaded.", _settingsManager.GlobalSettings.ServerName,
                    Notice.Count());
        }
    }
}