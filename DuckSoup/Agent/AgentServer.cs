using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DuckSoup.Library;
using DuckSoup.Library.Database.Models;
using SilkroadSecurityAPI;

// ReSharper disable UnusedVariable

#pragma warning disable 1998

namespace DuckSoup.Agent
{
    public class AgentServer : AsyncServer
    {
        public AgentServer(SettingsManager settingsManager) : base(settingsManager)
        {
            PacketHandler = new PacketHandler(settingsManager.AgentSettings.ClientWhitelist,
                settingsManager.AgentSettings.ClientBlacklist);

            // Register all handlers here
            // Mainly Exploits
            PacketHandler.RegisterClientHandler(0x7001,
                AGENT_CHARACTER_SELECTION_JOIN); // charname modify and not really logged in Exploit - 0x7001 - https://www.elitepvpers.com/forum/sro-pserver-guides-releases/4232366-release-disconnect-players-exploit-found-iwa-4.html 
            PacketHandler.RegisterClientHandler(0x705E,
                AGENT_SIEGE_ACTION); // SQL Injection - 0x705E - Also contains Tax / checkout checks - https://www.elitepvpers.com/forum/sro-private-server/4141360-information-sql-injection-ingame.html
            PacketHandler.RegisterClientHandler(0x70F9,
                AGENT_GUILD_UPDATE_NOTICE); // Guild Notice - 0x70F9 - Better safe than sorry.
            PacketHandler.RegisterClientHandler(0x34A9,
                AGENT_MAGICOPTION_GRANT); // Avatar Exploit - 0x34A9 - https://www.elitepvpers.com/forum/sro-pserver-guides-releases/3991992-release-invincible-avatar-magopt-exploit-3.html
            PacketHandler.RegisterClientHandler(0x7005,
                AGENT_LOGOUT); // [x] Crash Exploit - 0x7005 - https://www.elitepvpers.com/forum/sro-pserver-guides-releases/4232366-release-disconnect-players-exploit-found-iwa.html
            PacketHandler.RegisterClientHandler(0x70A7,
                CLIENT_PLAYER_BERSERK); // Zerk Exploit - 0x70A7 - https://www.elitepvpers.com/forum/sro-pserver-guides-releases/3991992-release-invincible-avatar-magopt-exploit-3.html
            PacketHandler.RegisterClientHandler(0x70A2,
                AGENT_SKILL_MASTERY_LEARN); // Skill Exploit - 0x70A2 - https://www.maxigame.com/forum/t/251583-meshur-vsro-mastery-exploit-ini-delirius-engelleme
            PacketHandler.RegisterClientHandler(0x3510,
                CLIENT_EXPLOIT_GSCRASH); // GS Crash Exploit - 0x3510 - https://www.elitepvpers.com/forum/sro-pserver-guides-releases/4383384-release-fix-gameserver-crash-runtime-error-exploit.html
            PacketHandler.RegisterModuleHandler(0xA103, AGENT_AUTH); // Exploit Prevention

            // Mainly Data / Information
            //PacketHandler.RegisterModuleHandler(0x3013, SERVER_AGENT_CHARACTER_DATA); // has a TODO
            PacketHandler.RegisterClientHandler(0x3012, AGENT_GAME_READY); // GameReady true
            PacketHandler.RegisterClientHandler(0x705A, AGENT_TELEPORT_USE); // GameReady false
            PacketHandler.RegisterModuleHandler(0x3020, AGENT_ENVIRONMENT_CELESTIAL_POSITION); // CharacterUniqueId

            // Mainly Features
            // Login Notice
            // Item control
            //PacketHandler.RegisterClientHandler(0x704C, AGENT_INVENTORY_ITEM_USE_CLIENT);
            //PacketHandler.RegisterModuleHandler(0xB04C, AGENT_INVENTORY_ITEM_USE_SERVER);
            // Union Limit - Invite: 0x70FB
            // PacketHandler.RegisterClientHandler(0x70FB, AGENT_GUILD_UNION_INVITE);
            // Guild Limit - Invite: 0x70F3
            //PacketHandler.RegisterClientHandler(0x70F3, AGENT_GUILD_INVITE);
            // QuestHelper Mark - Unique Targetting
            //PacketHandler.RegisterClientHandler(0x7402, CLIENT_QUESTMARK);
            // Unique Logging
            //PacketHandler.RegisterModuleHandler(0x300C, AGENT_GAME_NOTIFY);
            // Premium use item thing packet
            //serverXServerEngine.PacketManager.RegisterClientHandler(0x715F, TestPackets.Handle);

            // Chat
            //PacketHandler.RegisterClientHandler(0x7025, AGENT_CHAT);

            PacketHandler.RegisterModuleHandler(0xB021, AGENT_MOVEMENT_SERVER);
            PacketHandler.RegisterClientHandler(0x7021, AGENT_MOVEMENT_CLIENT);

            // Hwid
            PacketHandler.RegisterClientHandler(0x4000, Hwid);
            PacketHandler.RegisterClientHandler(0x4001, RequestHwidPacket);
        }

        private async Task<PacketResult> SERVER_AGENT_CHARACTER_DATA(Packet packet, Session session)
        {
            var serverTime = packet.ReadUInt32(); // * 4   uint    ServerTime               //SROTimeStamp
            var refObjId = packet.ReadUInt32(); // 4   uint    RefObjID
            var scale = packet.ReadUInt8(); // 1   byte    Scale
            var curLevel = packet.ReadUInt8(); // 1   byte    CurLevel
            var maxLevel = packet.ReadUInt8(); // 1   byte    MaxLevel
            var expOffset = packet.ReadUInt64(); // 8   ulong   ExpOffset
            var sExpOffset = packet.ReadUInt32(); // 4   uint    SExpOffset
            var remainGold = packet.ReadUInt64(); // 8   ulong   RemainGold
            var remainSkillPoint = packet.ReadUInt32(); // 4   uint    RemainSkillPoint
            var remainStatPoint = packet.ReadUInt16(); // 2   ushort  RemainStatPoint
            var remainHwanCount = packet.ReadUInt8(); // 1   byte    RemainHwanCount
            var gatheredExpPoint = packet.ReadUInt32(); // 4   uint    GatheredExpPoint
            var hp = packet.ReadUInt32(); // 4   uint    HP
            var mp = packet.ReadUInt32(); // 4   uint    MP
            var autoInverstExp = packet.ReadUInt8(); // 1   byte    AutoInverstExp
            var dailyPk = packet.ReadUInt8(); // 1   byte    DailyPK
            var totalPk = packet.ReadUInt16(); // 2   ushort  TotalPK
            var pkPenaltyPoint = packet.ReadUInt32(); // 4   uint    PKPenaltyPoint
            var hwanLevel = packet.ReadUInt8(); // 1   byte    HwanLevel
            var freePvp =
                packet.ReadUInt8(); // 1   byte    FreePVP           //0 = None, 1 = Red, 2 = Gray, 3 = Blue, 4 = White, 5 = Gold

            // //Inventory
            var inventorySize = packet.ReadUInt8(); // 1   byte    Inventory.Size
            var inventoryItemCount = packet.ReadUInt8(); // 1   byte    Inventory.ItemCount
            for (var i = 0; i < inventoryItemCount; i++) // for (int i = 0; i < Inventory.ItemCount; i++)
            {
                var itemSlot = packet.ReadUInt8(); //     1   byte    item.Slot
                var itemRentType = packet.ReadUInt32(); //     4   uint    item.RentType
                if (itemRentType == 1)
                {
                    var itemRentInfoCanDelete = packet.ReadUInt16(); //         2   ushort  item.RentInfo.CanDelete
                    var itemRentInfoPeriodBeginTime =
                        packet.ReadUInt32(); //         4   uint    item.RentInfo.PeriodBeginTime
                    var itemRentInfoPeriodEndTime =
                        packet.ReadUInt32(); //         4   uint    item.RentInfo.PeriodEndTime        
                }
                else if (itemRentType == 2)
                {
                    var itemRentInfoCanDelete = packet.ReadUInt16(); //         2   ushort  item.RentInfo.CanDelete
                    var itemRentInfoCanRecharge = packet.ReadUInt16(); //         2   ushort  item.RentInfo.CanRecharge
                    var itemRentInfoMeterRateTime =
                        packet.ReadUInt32(); //         4   uint    item.RentInfo.MeterRateTime        
                }
                else if (itemRentType == 3)
                {
                    var itemRentInfoCanDelete = packet.ReadUInt16(); //         2   ushort  item.RentInfo.CanDelete
                    var itemRentInfoCanRecharge = packet.ReadUInt16(); //         2   ushort  item.RentInfo.CanRecharge
                    var itemRentInfoPeriodBeginTime =
                        packet.ReadUInt32(); //         4   uint    item.RentInfo.PeriodBeginTime
                    var itemRentInfoPeriodEndTime =
                        packet.ReadUInt32(); //         4   uint    item.RentInfo.PeriodEndTime   
                    var itemRentInfoPackingTime =
                        packet.ReadUInt32(); //         4   uint    item.RentInfo.PackingTime        
                }

                var itemRefItemId = packet.ReadUInt32(); //     4   uint    item.RefItemID

                Logger.DebugFormat("itemId: {0} - itemSlot: {1} - itemRentType: {2}", itemRefItemId, itemSlot,
                    itemRentType); // debug
                RefObjCommon item = null;
                foreach (var var in Program.DatabaseManager.RefObjCommon.Where(var => var.ID == itemRefItemId))
                {
                    item = var;
                }

                if (item == null) continue;
                if (item.TypeID1 == 3)
                {
                    //ITEM_        
                    if (item.TypeID2 == 1)
                    {
                        //ITEM_CH
                        //ITEM_EU
                        //AVATAR_
                        var itemOptLevel = packet.ReadUInt8(); // 1   byte    item.OptLevel
                        var itemVariance = packet.ReadUInt64(); // 8   ulong   item.Variance
                        var itemData = packet.ReadUInt32(); // 4   uint    item.Data       //Durability
                        var itemMagParamNum = packet.ReadUInt8(); // 1   byte    item.MagParamNum
                        for (var paramIndex = 0; paramIndex < itemMagParamNum; paramIndex++)
                        {
                            var magParamType = packet.ReadUInt32(); // 4   uint    magParam.Type
                            var magParamValue = packet.ReadUInt32(); // 4   uint    magParam.Value                
                        }

                        var bindingOptionType = packet.ReadUInt8(); // 1   byte    bindingOptionType   //1 = Socket
                        var bindingOptionCount = packet.ReadUInt8(); // 1   byte    bindingOptionCount
                        for (var bindingOptionIndex = 0; bindingOptionIndex < bindingOptionCount; bindingOptionIndex++)
                        {
                            var bindingOptionSlot = packet.ReadUInt8(); // 1   byte bindingOption.Slot
                            var bindingOptionId = packet.ReadUInt32(); // 4   uint bindingOption.ID
                            var bindingOptionParam1 = packet.ReadUInt32(); // 4   uint bindingOption.nParam1
                        }

                        var bindingOptionType2 =
                            packet.ReadUInt8(); // 1   byte    bindingOptionType   //2 = Advanced elixir
                        var bindingOptionCount2 = packet.ReadUInt8(); // 1   byte    bindingOptionCount2
                        for (var bindingOptionIndex = 0; bindingOptionIndex < bindingOptionCount2; bindingOptionIndex++)
                        {
                            var bindingOptionSlot = packet.ReadUInt8(); // 1   byte bindingOption.Slot
                            var bindingOptionId = packet.ReadUInt32(); // 4   uint bindingOption.ID
                            var bindingOptionOptValue = packet.ReadUInt32(); // 4   uint bindingOption.OptValue
                        }
                    }
                    else if (item.TypeID2 == 2)
                    {
                        // TODO: COS is broken, needs fixing, else everything beyond this wont work properly
                        if (item.TypeID3 == 1)
                        {
                            //ITEM_COS_P
                            var cosState = packet.ReadUInt8(); //1   byte    State
                            var cosRefObjId = packet.ReadUInt8(); // 4 uint RefObjID
                            var cosName = packet.ReadAscii(); // 2 ushort Name.Length //     * string Name
                            if (item.TypeID4 == 2)
                            {
                                //ITEM_COS_P (Ability)
                                var cosSecondsToRentEndTime = packet.ReadUInt32(); // 4 uint SecondsToRentEndTime
                            }

                            packet.ReadUInt8(); // 1 byte unkByte0
                        }
                        else if (item.TypeID3 == 2)
                        {
                            //ITEM_ETC_TRANS_MONSTER
                            var etcRefObjId = packet.ReadUInt32(); // 4   uint    RefObjID
                        }
                        else if (item.TypeID3 == 3)
                        {
                            //MAGIC_CUBE
                            var quantity =
                                packet
                                    .ReadUInt32(); // 4   uint    Quantity        //Do not confuse with StackCount, this indicates the amount of elixirs in the cube
                        }
                    }
                    else if (item.TypeID2 == 3)
                    {
                        //ITEM_ETC
                        var itemStackCount = packet.ReadUInt16(); // 2   ushort  item.StackCount

                        if (item.TypeID3 == 11)
                        {
                            if (item.TypeID4 == 1 || item.TypeID4 == 2)
                            {
                                //MAGICSTONE, ATTRSTONE
                                var attributeAssimilationProbability =
                                    packet.ReadUInt8(); // 1   byte    AttributeAssimilationProbability
                            }
                        }
                        else if (item.TypeID3 == 14 && item.TypeID4 == 2)
                        {
                            //ITEM_MALL_GACHA_CARD_WIN
                            //ITEM_MALL_GACHA_CARD_LOSE
                            var magParamNum = packet.ReadUInt8(); // 1   byte    item.MagParamCount
                            for (var paramIndex = 0; paramIndex < magParamNum; paramIndex++)
                            {
                                var magParamType = packet.ReadUInt32(); //4   uint magParam.Type
                                var magParamValue = packet.ReadUInt32(); //4   uint magParam.Value
                            }
                        }
                    }
                }
            }

            Logger.InfoFormat("Inventory Done {0} - {1}", inventoryItemCount, inventorySize); // debug

            //AvatarInventory
            var avatarInventorySize = packet.ReadUInt8(); // 1 byte AvatarInventory.Size
            var avatarInventoryItemCount = packet.ReadUInt8(); // 1 byte AvatarInventory.ItemCount
            for (var i = 0; i < avatarInventoryItemCount; i++)
            {
                packet.ReadUInt8(); // 1 byte item.Slot
                var itemRentType = packet.ReadUInt32(); // 4 uint item.RentType
                if (itemRentType == 1)
                {
                    packet.ReadUInt16(); // 2 ushort item.RentInfo.CanDelete
                    packet.ReadUInt16(); // 4 uint item.RentInfo.PeriodBeginTime
                    packet.ReadUInt32(); // 4 uint item.RentInfo.PeriodEndTime
                }
                else if (itemRentType == 2)
                {
                    packet.ReadUInt16(); // 2 ushort item.RentInfo.CanDelete
                    packet.ReadUInt16(); // 2 ushort item.RentInfo.CanRecharge
                    packet.ReadUInt32(); // 4 uint item.RentInfo.MeterRateTime
                }
                else if (itemRentType == 3)
                {
                    packet.ReadUInt16(); // 2 ushort item.RentInfo.CanDelete
                    packet.ReadUInt16(); // 2 ushort item.RentInfo.CanRecharge
                    packet.ReadUInt32(); // 4 uint item.RentInfo.PeriodBeginTime
                    packet.ReadUInt32(); // 4 uint item.RentInfo.PeriodEndTime
                    packet.ReadUInt32(); // 4 uint item.RentInfo.PackingTime
                }

                var itemRefItemId = packet.ReadUInt32(); // 4 uint item.RefItemID
                RefObjCommon item = null;
                foreach (var var in Program.DatabaseManager.RefObjCommon.Where(var => var.ID == itemRefItemId))
                {
                    item = var;
                }

                if (item.TypeID1 == 3)
                {
                    //ITEM_        
                    if (item.TypeID2 == 1) //TODO: Narrow filters for AvatarInventory
                    {
                        //ITEM_CH
                        //ITEM_EU
                        //AVATAR_
                        packet.ReadUInt8(); // 1 byte item.OptLevel
                        packet.ReadUInt64(); // 8 ulong item.Variance
                        packet.ReadUInt32(); // 4 uint item.Data //Durability
                        var itemMagParamNum = packet.ReadUInt8(); // 1 byte item.MagParamNum
                        for (var paramIndex = 0; paramIndex < itemMagParamNum; paramIndex++)
                        {
                            packet.ReadUInt32(); // 4 uint magParam.Type
                            packet.ReadUInt32(); // 4 uint magParam.Value
                        }

                        packet.ReadUInt8(); // 1 byte bindingOptionType //1 = Socket
                        var bindingOptionCount = packet.ReadUInt8(); // 1 byte bindingOptionCount
                        for (var bindingOptionIndex = 0;
                            bindingOptionIndex < bindingOptionCount;
                            bindingOptionIndex++)
                        {
                            packet.ReadUInt8(); // 1 byte bindingOption.Slot
                            packet.ReadUInt32(); // 4 uint bindingOption.ID
                            packet.ReadUInt32(); // 4 uint bindingOption.nParam1
                        }

                        packet.ReadUInt8(); // 1 byte bindingOptionType //2 = Advanced elixir
                        var bindingOptionCount2 = packet.ReadUInt8(); // 1 byte bindingOptionCount
                        for (var bindingOptionIndex = 0;
                            bindingOptionIndex < bindingOptionCount2;
                            bindingOptionIndex++)
                        {
                            packet.ReadUInt8(); // 1 byte bindingOption.Slot
                            packet.ReadUInt32(); // 4 uint bindingOption.ID
                            packet.ReadUInt32(); // 4 uint bindingOption.OptValue
                        }
                    }
                }
            }

            Logger.InfoFormat("AvatarInventory Done {0} - {1}", avatarInventoryItemCount,
                avatarInventorySize); // debug


            packet.ReadUInt8(); //1 byte unkByte1 //not a counter

            //Masteries
            var nextMastery = packet.ReadUInt8(); // 1   byte    nextMastery
            while (nextMastery == 1)
            {
                var masteryId = packet.ReadUInt32(); // 4   uint    mastery.ID
                var masteryLevel = packet.ReadUInt8(); // 1   byte    mastery.Level   

                nextMastery = packet.ReadUInt8(); // 1   byte    nextMastery

                Logger.InfoFormat("Mastery {0} - {1}", masteryId, masteryLevel); // debug
            }

            packet.ReadUInt8(); // 1   byte    unkByte2    //not a counter
            Logger.InfoFormat("Masterys Done"); // debug

            //Skills
            var nextSkill = packet.ReadUInt8(); // 1   byte    nextSkill
            while (nextSkill == 1)
            {
                var skillId = packet.ReadUInt32(); // 4   uint    skill.ID
                var skillEnabled = packet.ReadUInt8(); // 1   byte    skill.Enabled   

                nextSkill = packet.ReadUInt8(); // 1   byte    nextSkill
                Logger.InfoFormat("Skill {0} - {1}", skillId, skillEnabled); // debug
            }

            Logger.Info("Skills Done"); // debug

            //Quests
            var completedQuestCount = packet.ReadUInt16(); // 2   ushort  CompletedQuestCount
            var completedQuests = packet.ReadUInt32Array(completedQuestCount); // *   uint[]  CompletedQuests

            var activeQuestCount = packet.ReadUInt8(); // 1   byte    ActiveQuestCount
            for (var activeQuestIndex = 0; activeQuestIndex < activeQuestCount; activeQuestIndex++)
            {
                var questRefQuestId = packet.ReadUInt32(); // 4   uint    quest.RefQuestID
                var questAchievementCount = packet.ReadUInt8(); // 1   byte    quest.AchievementCount
                var questRequiresAutoShareParty = packet.ReadUInt8(); // 1   byte    quest.RequiresAutoShareParty
                var questType = packet.ReadUInt8(); // 1   byte    quest.Type
                if (questType == 28)
                {
                    var questRemainingTime = packet.ReadUInt32(); // 4   uint    remainingTime
                }

                var questStatus = packet.ReadUInt8(); // 1   byte    quest.Status

                if (questType != 8)
                {
                    var questObjectiveCount = packet.ReadUInt8(); // 1   byte    quest.ObjectiveCount
                    for (var objectiveIndex = 0; objectiveIndex < questObjectiveCount; objectiveIndex++)
                    {
                        var questObjectiveId = packet.ReadUInt8(); // 1   byte    objective.ID
                        var questObjectiveStatus =
                            packet.ReadUInt8(); // 1   byte    objective.Status        //0 = Done, 1  = On
                        var questObjectiveName =
                            packet.ReadAscii(); // 2   ushort  objective.Name.Length // *   string  objective.Name
                        var objectiveTaskCount = packet.ReadUInt8(); // 1   byte    objective.TaskCount
                        for (var taskIndex = 0; taskIndex < objectiveTaskCount; taskIndex++)
                        {
                            var questTaskValue = packet.ReadUInt32(); // 4   uint    task.Value
                        }
                    }
                }

                if (questType == 88)
                {
                    var refObjCount = packet.ReadUInt8(); // 1   byte    RefObjCount
                    for (var refObjIndex = 0; refObjIndex < refObjCount; refObjIndex++)
                    {
                        packet.ReadUInt32(); // 4   uint    RefObjID    //NPCs
                    }
                }
            }

            packet.ReadUInt8(); // 1   byte    unkByte3        //Structure changes!!!

            Logger.InfoFormat("Quest Done {0}", activeQuestCount); // debug

            //CollectionBook
            var startedCollectionCount = packet.ReadUInt32(); // 4   uint    CollectionBookStartedThemeCount
            for (var i = 0; i < startedCollectionCount; i++)
            {
                var themeIndex = packet.ReadUInt32(); // 4   uint    theme.Index
                var themeStartedDateTime = packet.ReadUInt32(); // 4   uint    theme.StartedDateTime   //SROTimeStamp
                var themePages = packet.ReadUInt32(); // 4   uint    theme.Pages
            }

            var uniqueId = packet.ReadUInt32(); // 4   uint    UniqueID

            //Position
            var positionRegionId = packet.ReadUInt16(); // 2   ushort  Position.RegionID
            var positionX = packet.ReadFloat(); // 4   float   Position.X
            var positionY = packet.ReadFloat(); // 4   float   Position.Y
            var positionZ = packet.ReadFloat(); // 4   float   Position.Z
            var positionAngle = packet.ReadUInt16(); // 2   ushort  Position.Angle

            Logger.InfoFormat("Position {0}, {1}, {2}, {3}", positionRegionId, positionX, positionY,
                positionZ); // debug

            //Movement
            var movementHasDestination = packet.ReadUInt8(); // 1   byte    Movement.HasDestination
            var movementType = packet.ReadUInt8(); // 1   byte    Movement.Type
            if (movementHasDestination == 1)
            {
                var movementDestionationRegion = packet.ReadUInt16(); // 2   ushort  Movement.DestinationRegion        
                if (positionRegionId < short.MaxValue)
                {
                    //World
                    var movementDestinationOffsetX = packet.ReadUInt16(); // 2   ushort  Movement.DestinationOffsetX
                    var movementDestinationOffsetY = packet.ReadUInt16(); // 2   ushort  Movement.DestinationOffsetY
                    var movementDestinationOffsetZ = packet.ReadUInt16(); // 2   ushort  Movement.DestinationOffsetZ
                }
                else
                {
                    //Dungeon
                    var movementDestinationOffsetX = packet.ReadUInt32(); // 4   uint  Movement.DestinationOffsetX
                    var movementDestinationOffsetY = packet.ReadUInt32(); // 4   uint  Movement.DestinationOffsetY
                    var movementDestinationOffsetZ = packet.ReadUInt32(); // 4   uint  Movement.DestinationOffsetZ
                }
            }
            else
            {
                var movementSource =
                    packet.ReadUInt8(); // 1   byte    Movement.Source     //0 = Spinning, 1 = Sky-/Key-walking
                var movementAngle =
                    packet.ReadUInt16(); // 2   ushort  Movement.Angle      //Represents the new angle, character is looking at
            }

            //State
            var lifeState = packet.ReadUInt8(); // 1   byte    State.LifeState         //1 = Alive, 2 = Dead
            packet.ReadUInt8(); // 1   byte    State.unkByte0
            var stateMotionState =
                packet.ReadUInt8(); // 1   byte    State.MotionState       //0 = None, 2 = Walking, 3 = Running, 4 = Sitting
            var
                stateStatus =
                    packet.ReadUInt8(); // 1   byte    State.Status            //0 = None, 1 = Hwan, 2 = Untouchable, 3 = GameMasterInvincible, 5 = GameMasterInvisible, 5 = ?, 6 = Stealth, 7 = Invisible
            var stateWalkSpeed = packet.ReadFloat(); // 4   float   State.WalkSpeed
            var stateRunSpeed = packet.ReadFloat(); // 4   float   State.RunSpeed
            var stateHwanSpeed = packet.ReadFloat(); // 4   float   State.HwanSpeed
            var stateBuffCount = packet.ReadUInt8(); // 1   byte    State.BuffCount

            for (var i = 0; i < stateBuffCount; i++)
            {
                var buffRefSkillId = packet.ReadUInt32(); // 4   uint    Buff.RefSkillID
                var buffDuration = packet.ReadUInt32(); // 4   uint    Buff.Duration

                RefSkill skill = null;
                foreach (var var in Program.DatabaseManager.RefSkill.Where(var => var.ID == buffRefSkillId))
                {
                    skill = var;
                }

                if (skill == null) continue;
                if (skill.ParamsContains(1701213281))
                {
                    //1701213281 -> atfe -> "auto transfer effect" like Recovery Division
                    var isCreator = packet.ReadUInt8(); // 1   bool    IsCreator
                }
            }

            var name = packet.ReadAscii(); // 2   ushort  Name.Length // *   string  Name
            var jobName = packet.ReadAscii(); // 2   ushort  JobName.Length // *   string  JobName
            var jobType = packet.ReadUInt8(); // 1   byte    JobType
            var jobLevel = packet.ReadUInt8(); // 1   byte    JobLevel
            var jobExp = packet.ReadUInt32(); // 4   uint    JobExp
            var jobContribution = packet.ReadUInt32(); // 4   uint    JobContribution
            var jobReward = packet.ReadUInt32(); // 4   uint    JobReward
            var pvpState = packet.ReadUInt8(); // 1   byte    PVPState                //0 = White, 1 = Purple, 2 = Red
            var transportFlag = packet.ReadUInt8(); // 1   byte    TransportFlag
            var inCombat = packet.ReadUInt8(); // 1   byte    InCombat
            if (transportFlag == 1)
            {
                var transportUniquedId = packet.ReadUInt32(); // 4   uint    Transport.UniqueID
            }

            var pvpFlag =
                packet.ReadUInt8(); // 1   byte    PVPFlag                 //0 = Red Side, 1 = Blue Side, 0xFF = None
            var guideFlag = packet.ReadUInt64(); // 8   ulong   GuideFlag
            var jid = packet.ReadUInt32(); // 4   uint    JID
            var gmFlag = packet.ReadUInt8(); // 1   byte    GMFlag

            var activationFlag =
                packet.ReadUInt8(); // 1   byte    ActivationFlag          //ConfigType:0 --> (0 = Not activated, 7 = activated)
            var hotkeyCount = packet.ReadUInt8(); // 1   byte    Hotkeys.Count           //ConfigType:1
            for (var i = 0; i < hotkeyCount; i++)
            {
                var hotkeySlotSeq = packet.ReadUInt8(); // 1   byte    hotkey.SlotSeq
                var hotkeySlotContentType = packet.ReadUInt8(); // 1   byte    hotkey.SlotContentType
                var hotkeySlotData = packet.ReadUInt32(); // 4   uint    hotkey.SlotData
            }

            var autoHpConfig = packet.ReadUInt16(); // 2   ushort  AutoHPConfig            //ConfigType:11
            var autoMpConfig = packet.ReadUInt16(); // 2   ushort  AutoMPConfig            //ConfigType:12
            var autoUniversalConfig = packet.ReadUInt16(); // 2   ushort  AutoUniversalConfig     //ConfigType:13
            var autoPotionDelay = packet.ReadUInt8(); // 1   byte    AutoPotionDelay         //ConfigType:14

            var blockedWhisperCount = packet.ReadUInt8(); // 1   byte    blockedWhisperCount
            for (var i = 0; i < blockedWhisperCount; i++)
            {
                var target = packet.ReadAscii(); // 2   ushort  Target.Length // *   string  Target
            }

            packet.ReadUInt32(); // 4   uint    unkUShort0      //Structure changes!!!
            packet.ReadUInt8(); // 1   byte    unkByte4        //Structure changes!!!

            Logger.InfoFormat("hotkeycount: {0}", hotkeyCount); // debug

            return new PacketResult(PacketResultType.Nothing);
        }

        private async Task<PacketResult> AGENT_MOVEMENT_CLIENT(Packet packet, Session session)
        {
            var flag = packet.ReadUInt8();
            var sectorX = packet.ReadUInt8();
            var sectorY = packet.ReadUInt8();

            if (flag != 1) return new PacketResult(PacketResultType.Nothing);

            session.SectorX = sectorX;
            session.SectorY = sectorY;
            session.PositionX = packet.ReadUInt16();
            session.PositionY = packet.ReadUInt16();
            session.PositionZ = packet.ReadUInt16();

            return new PacketResult(PacketResultType.Nothing);
        }

        private async Task<PacketResult> AGENT_MOVEMENT_SERVER(Packet packet, Session session)
        {
            var target = packet.ReadUInt32(); // Unique ID from player

            if (target != session.UniqueCharId) return new PacketResult(PacketResultType.Nothing);

            // sky = 0, ground = 1
            packet.ReadUInt8(); //sky or ground click
            var region = packet.ReadUInt16(); // Region ID

            var regionLength = region.ToString().Length; // Some weird Regionthing

            // Make sure region is 5 numbers.
            if (regionLength == 5 && packet.GetBytes().Length == 24)
                session.LatestRegionId = region;

            return new PacketResult(PacketResultType.Nothing);
        }

        // private async Task<PacketResult> AGENT_CHAT(Packet packet, Session session)
        // {
        //     return new PacketResult(PacketResultType.Nothing);
        // }

        // private async Task<PacketResult> AGENT_GAME_NOTIFY(Packet packet, Session session)
        // {
        //     foreach (var agentServer in ServerManager.AgentServers)
        //     {
        //         if (session.ClientId == agentServer.Sessions.First().ClientId)
        //         {
        //             int type = packet.ReadInt8(); // 0x05 == spawn, 0x06 == kill
        //             packet.ReadInt8();
        //             var mobId = packet.ReadInt32();
        //             var mobCodename = Task.Run(() =>
        //                 ServerManager.DatabaseManager.Database.ExecuteScalarAsync<string>(
        //                     "Select CodeName128 from SRO_VT_SHARD.dbo._RefObjCommon where id = @0", mobId)).Result;
        //             switch (type)
        //             {
        //                 // spawn
        //                 case 0x05:
        //                     Task.Run(() =>
        //                         ServerManager.DatabaseManager.Database.ExecuteAsync(
        //                             "INSERT INTO SRO_VT_PROXY.dbo._LogUniqueSpawn VALUES (@0, GETDATE());",
        //                             mobCodename));
        //                     break;
        //                 // kill
        //                 case 0x06:
        //                 {
        //                     var name = packet.ReadAscii();
        //                     Task.Run(() =>
        //                         ServerManager.DatabaseManager.Database.ExecuteAsync(
        //                             "INSERT INTO SRO_VT_PROXY.dbo._LogUniqueKills VALUES (@0, @1, GETDATE());", name,
        //                             mobCodename));
        //                     break;
        //                 }
        //             }
        //         }
        //     
        //         break;
        //     }
        //
        //     return new PacketResult(PacketResultType.Nothing);
        // }

        // private async Task<PacketResult> CLIENT_QUESTMARK(Packet packet, Session session)
        // {
        //     return new PacketResult(PacketResultType.Block);
        // }

        private async Task<PacketResult> AGENT_TELEPORT_USE(Packet packet, Session session)
        {
            session.CharacterGameReady = false;
            return new PacketResult(PacketResultType.Nothing);
        }

        // private async Task<PacketResult> AGENT_GUILD_INVITE(Packet packet, Session session)
        // {
        //     var check = session.GetUnionMembersCount() >= Settings.GuildLimit;
        //     
        //     if (!check && Settings.GuildLimit != 1 && Settings.GuildLimit != 0)
        //         return new PacketResult(PacketResultType.Nothing);
        //     
        //     session.SendNotice(Settings.GuildLimitNotice);
        //     return new PacketResult(PacketResultType.Block);
        // }

        // private async Task<PacketResult> AGENT_GUILD_UNION_INVITE(Packet packet, Session session)
        // {
        //     var check = session.GetUnionMembersCount() >= Settings.UnionLimit;
        //     
        //     if (!check) return new PacketResult(PacketResultType.Nothing);
        //     
        //     session.SendNotice(Settings.UnionLimitNotice);
        //     return new PacketResult(PacketResultType.Block);
        // }

        // private async Task<PacketResult> AGENT_INVENTORY_ITEM_USE_SERVER(Packet packet, Session session)
        // {
        //     // var status = packet.ReadUInt8();
        //     // if (status != 1) return new PacketResult(PacketResultType.Nothing);
        //     //
        //     // packet.ReadUInt8(); // unk1
        //     // var unk2 = packet.ReadUInt16();
        //     // var unk3 = packet.ReadUInt16();
        //     //
        //     // if (unk2 == 1 && unk3 == 4301)
        //     // {
        //     //     if (session.HasJobState())
        //     //         return new PacketResult(PacketResultType.Nothing);
        //     //
        //     //     session.SendNotice(Settings.SecondPetPageReload);
        //     //
        //     //     Task.Run(() =>
        //     //         ServerManager.DatabaseManager.Database.ExecuteAsync(
        //     //             "INSERT INTO SR_ADDON_DB.dbo._InstantCharReloadDelivery (CharID) VALUES (@0);",
        //     //             session.Charid));
        //     // }
        //
        //     return new PacketResult(PacketResultType.Nothing);
        // }

        // private async Task<PacketResult> AGENT_INVENTORY_ITEM_USE_CLIENT(Packet packet, Session session)
        // {
        //      0x704C Client Use Inventory
        //      byte slot
        //      ushort itemType
        //      uint uniqueID
        //     
        //     byte slot = packet.ReadUInt8();
        //     byte type = packet.ReadUInt8();
        //     byte typeid = packet.ReadUInt8();
        //     
        //     // Reverse - slot 16 - itemType 236 - uniqueId 25 - remain 5
        //     // Global - slot 43 - itemType 237 - uniqueId 41 - remain 6
        //     /*
        //      * EVENT_RENT    - slot 37 - type 236 - typeid 25 - remain 5 (1)- packetsize 8 (4 wenn last / deathpoint)
        //      * EVENT         - slot 38 - type 236 - typeid 25 - remain 5 (1)- packetsize 8 (4 wenn last / deathpoint)
        //      * MALL          - slot 39 - type 237 - typeid 25 - remain 5 (1)- packetsize 8 (4 wenn last / deathpoint)
        //      */
        //     // reverse scrolls
        //     if (typeid != 25 || (type != 236 && type != 237)) return new PacketResult(PacketResultType.Nothing);
        //     
        //     var identifier = packet.ReadUInt8();
        //     if ((identifier == 3 || identifier == 2 || identifier == 7) &&
        //         DateTime.Now.Subtract(session.LastReverseTime).TotalSeconds > Settings.ReverseDelay)
        //     {
        //         session.LastReverseTime = DateTime.Now;
        //         return new PacketResult(PacketResultType.Nothing);
        //     }
        //     
        //     int timeleft = (int) (Settings.ReverseDelay - DateTime.Now.Subtract(session.LastReverseTime).TotalSeconds);
        //     session.SendNotice(Settings.ReverseDelayNotice.Replace("%time%", timeleft.ToString()));
        //     return new PacketResult(PacketResultType.Block);
        // }

        private async Task<PacketResult> AGENT_GAME_READY(Packet packet, Session session)
        {
            // fix to not crash on autonotice
            session.CharacterGameReady = true;

            //
            // if (session.CurLevel > 90)
            // {
            //     long exp = Task.Run(() =>
            //         ServerManager.DatabaseManager.Database.ExecuteScalarAsync<long>(
            //             "SELECT Exp_C FROM SRO_VT_SHARD.dbo._RefLevel WHERE lvl = 90;")).Result - 1;
            //
            //     Task.Run(() =>
            //         ServerManager.DatabaseManager.Database.ExecuteAsync(
            //             "UPDATE SRO_VT_SHARD.dbo._CharSkillMastery SET Level = 90 WHERE CharID = @0 AND Level > 90;",
            //             session.Charid));
            //
            //     Task.Run(() =>
            //         ServerManager.DatabaseManager.Database.ExecuteAsync(
            //             "UPDATE SRO_VT_SHARD.dbo._Char SET ExpOffset = @0, RemainStatPoint = 267, Strength = 20, Intellect = 20, CurLevel = 90, MaxLevel = 90 WHERE CharID = @1 AND CurLevel > 90;",
            //             exp, session.Charid));
            //
            //     Task.Run(() =>
            //         ServerManager.DatabaseManager.Database.ExecuteAsync(
            //             "UPDATE SRO_VT_SHARD.dbo._GuildMember SET CharLevel = '90' WHERE CharName = @0;", exp,
            //             session.Charname));
            //
            //     Task.Run(() =>
            //         ServerManager.DatabaseManager.Database.ExecuteAsync(
            //             "INSERT INTO SR_ADDON_DB.dbo._InstantCharReloadDelivery (CharID) VALUES (@0);",
            //             session.Charid));
            // }
            //
            // // Login Notice
            // if (Settings.LoginNoticeActive && !session.WelcomeNotice)
            // {
            //     session.WelcomeNotice = true;
            //     session.SendNotice(Settings.LoginNotice);
            // }
            //
            // if (session.GM)
            // {
            //     return new PacketResult(PacketResultType.Nothing);
            // }
            //
            // if (session.HasJobState())
            // {
            //     session.IsJobbing = true;
            //     var jobPerHwid = ServerManager.AgentServers.SelectMany(agentServer => agentServer.Sessions)
            //         .Count(agentServerSession =>
            //             agentServerSession.Hwid == session.Hwid && agentServerSession.IsJobbing);
            //     var jobPerIp = ServerManager.AgentServers.SelectMany(agentServer => agentServer.Sessions)
            //         .Count(agentServerSession =>
            //             agentServerSession.ClientIp == session.ClientIp && agentServerSession.IsJobbing);
            //
            //     if (Settings.JobPerHwid != 0)
            //     {
            //         if (jobPerHwid > Settings.JobPerHwid)
            //         {
            //             session.SendNotice(Settings.JobPerHwidExceeded);
            //             Task.Delay(1000).ContinueWith(t => session.Destroy());
            //         }
            //
            //         if (jobPerIp > Settings.JobPerHwid)
            //         {
            //             var otherusername = "";
            //             foreach (var agentServerSession in ServerManager.AgentServers.SelectMany(agentServer =>
            //                 agentServer.Sessions.Where(agentServerSession =>
            //                     agentServerSession.ClientIp == session.ClientIp &&
            //                     agentServerSession.Username != session.Username && agentServerSession.IsJobbing)))
            //             {
            //                 otherusername = agentServerSession.Username;
            //             }
            //
            //             //_LogIpJobbing
            //             Task.Run(() =>
            //                 ServerManager.DatabaseManager.Database.ExecuteAsync(
            //                     "INSERT INTO SRO_VT_PROXY.dbo._LogIpJobbing VALUES (@0, @1, @2, GETDATE());",
            //                     session.Username,
            //                     otherusername, session.ClientIp));
            //         }
            //     }
            // }
            // else
            // {
            //     session.IsJobbing = false;
            // }
            //
            // if (session.HwidCheck)
            // {
            //     // Fixes permanent reapply of hwid checks
            //     return new PacketResult(PacketResultType.Nothing);
            // }
            //
            // if (Settings.HwidWhitelist.Contains(session.Username))
            // {
            //     return new PacketResult(PacketResultType.Nothing);
            // }
            //
            //
            // // if HWIDLimit is not turned off
            // if (Settings.HwidLimit != 0)
            // {
            //     var hwidcounter = ServerManager.AgentServers.Sum(agentServer =>
            //         agentServer.Sessions.Count(serverSession => serverSession.Hwid == session.Hwid));
            //
            //     if (hwidcounter == 0)
            //     {
            //         session.SendNotice(Settings.HwidEmpty);
            //         Task.Delay(1000).ContinueWith(t => session.Destroy());
            //     }
            //
            //     // hwid
            //     if (hwidcounter > Settings.HwidLimit)
            //     {
            //         session.SendNotice(Settings.HwidExceeded.Replace("%count%", Settings.HwidLimit.ToString()));
            //         Task.Delay(1000).ContinueWith(t => session.Destroy());
            //     }
            //
            //     session.HwidCheck = true;
            // }
            //
            // Task.Run(() =>
            //     ServerManager.DatabaseManager.Database.ExecuteAsync(
            //         "INSERT INTO SRO_VT_PROXY.dbo._LogHwid VALUES (@0, @1, @2, @3, @4, GETDATE());", session.Username,
            //         session.Charid, session.Charname, session.ClientIp,
            //         session.Hwid.Replace("{", "").Replace("}", "")));

            return new PacketResult(PacketResultType.Nothing);
        }

        private async Task<PacketResult> AGENT_ENVIRONMENT_CELESTIAL_POSITION(Packet packet, Session session)
        {
            session.UniqueCharId = packet.ReadUInt32();
            return new PacketResult(PacketResultType.Nothing);
        }

        private async Task<PacketResult> CLIENT_EXPLOIT_GSCRASH(Packet packet, Session session)
        {
            Global.Logger.WarnFormat("EXPLOIT - {0} tried to use GS_CRASH_EXPLOIT - {1:X}", session.Charname,
                packet.Opcode);
            return new PacketResult(PacketResultType.Disconnect);
        }

        private async Task<PacketResult> AGENT_SKILL_MASTERY_LEARN(Packet packet, Session session)
        {
            packet.ReadUInt32(); // masteryid
            var level = packet.ReadUInt8();

            if (level == 1) return new PacketResult(PacketResultType.Nothing);

            Global.Logger.WarnFormat("EXPLOIT - {0} tried to use SKILL_EXPLOIT - {1:X}", session.Charname,
                packet.Opcode);
            return new PacketResult(PacketResultType.Disconnect);
        }

        private async Task<PacketResult> CLIENT_PLAYER_BERSERK(Packet packet, Session session)
        {
            var flag = packet.ReadUInt8();
            if (flag == 1) return new PacketResult(PacketResultType.Nothing);

            Global.Logger.WarnFormat("EXPLOIT - {0} tried to use INVIS EXPLOIT - {1:X}", session.Charname,
                packet.Opcode);
            return new PacketResult(PacketResultType.Disconnect);
        }

        private async Task<PacketResult> AGENT_LOGOUT(Packet packet, Session session)
        {
            if (!session.CharScreen)
            {
                Global.Logger.WarnFormat("EXPLOIT - {0} tried to use AS_CRASH_EXPLOIT - {1:X} at 1",
                    session.Charname,
                    packet.Opcode);
                return new PacketResult(PacketResultType.Disconnect);
            }

            if (session.Charid <= 0)
            {
                Global.Logger.WarnFormat("EXPLOIT - {0} tried to use AS_CRASH_EXPLOIT - {1:X} at 2",
                    session.Charname,
                    packet.Opcode);
                return new PacketResult(PacketResultType.Disconnect);
            }

            if (packet.ReadUInt8() > 2)
            {
                Global.Logger.WarnFormat("EXPLOIT - {0} tried to use AS_CRASH_EXPLOIT - {1:X} at 3",
                    session.Charname,
                    packet.Opcode);
                return new PacketResult(PacketResultType.Disconnect);
            }

            return new PacketResult(PacketResultType.Nothing);
        }

        private async Task<PacketResult> AGENT_MAGICOPTION_GRANT(Packet packet, Session session)
        {
            var avatarBlue = packet.ReadAscii().ToLower();
            if (avatarBlue.Contains("avatar")) return new PacketResult(PacketResultType.Nothing);

            Global.Logger.WarnFormat("EXPLOIT - {0} tried to use AVATAR_EXPLOIT - {1:X}", session.Charname,
                packet.Opcode);
            return new PacketResult(PacketResultType.Disconnect);
        }

        private async Task<PacketResult> AGENT_GUILD_UPDATE_NOTICE(Packet packet, Session session)
        {
            var titleMessage = packet.ReadAscii();
            var informationMessage = packet.ReadAscii();

            if (!informationMessage.Contains("\'") && !informationMessage.Contains("\"") &&
                !informationMessage.Contains("-") && !titleMessage.Contains("\'") && !titleMessage.Contains("\"") &&
                !titleMessage.Contains("-")) return new PacketResult(PacketResultType.Nothing);
            Global.Logger.WarnFormat("EXPLOIT - {0} tried to use GUILD_SQL_INJECTION - {1:X}",
                session.Charname,
                packet.Opcode);
            await session.SendNotice(
                "You're not allowed to use special characters in this textfield. We've replaced them for you.");

            var newPck = new Packet(packet.Opcode, packet.Encrypted, packet.Massive);
            newPck.WriteAscii(titleMessage.Replace("\'", " ").Replace("-", " ").Replace("\"", " ").Replace(";", " "));
            newPck.WriteAscii(informationMessage.Replace("\'", " ").Replace("-", " ").Replace("\"", " ")
                .Replace(";", " "));

            return new PacketResult(newPck, PacketResultType.Override);
        }

        private async Task<PacketResult> AGENT_SIEGE_ACTION(Packet packet, Session session)
        {
            packet.ReadUInt32();
            var unk2 = packet.ReadUInt8();
            uint unk3 = 0;
            if (unk2 == 1 || unk2 == 2 || unk2 == 26)
            {
                unk3 = packet.ReadUInt32();
            }

            // tax percentage change
            // if (unk2 == 1 && unk3 == 1)
            // {
            //     packet.ReadUInt16();
            //     session.SendNotice(Settings.FwTaxManipulationNotice);
            //     return new PacketResult(PacketResultType.Block);
            // }

            // tax withdraw
            // if (unk2 == 2 && unk3 == 1)
            // {
            //     packet.ReadUInt64();
            //     session.SendNotice(Settings.FwWithdrawNotice);
            //     return new PacketResult(PacketResultType.Block);
            // }

            // About guild
            if (unk2 != 26 || unk3 != 1) return new PacketResult(PacketResultType.Nothing);
            var message = packet.ReadAscii();

            if (!message.Contains("\'") && !message.Contains("\"") && !message.Contains("-"))
                return new PacketResult(PacketResultType.Nothing);

            Global.Logger.WarnFormat("EXPLOIT - {0} tried to use FW_SQL_INJECTION - {1:X}",
                session.Charname,
                packet.Opcode);
            await session.SendNotice("You're not allowed to use special characters in this textfield.");
            return new PacketResult(PacketResultType.Block);
        }

        private async Task<PacketResult> AGENT_AUTH(Packet packet, Session session)
        {
            if (packet.ReadUInt8() == 1)
                session.UserLoggedIn = true;

            if (session.UserLoggedIn)
                session.CharScreen = true;

            return new PacketResult(PacketResultType.Nothing);
        }

        private async Task<PacketResult> AGENT_CHARACTER_SELECTION_JOIN(Packet packet, Session session)
        {
            if (session.CharnameSent)
                return new PacketResult(PacketResultType.Block);

            if (!session.CharScreen)
            {
                Global.Logger.WarnFormat("Client {0}({1}) attempted to send 0x7001 outside char screen!",
                    session.ClientId, session.ClientIp);
                return new PacketResult(PacketResultType.Disconnect);
            }

            session.Charname = packet.ReadAscii();

            if ((session.PacketLength - session.Charname.Length) != 2)
            {
                Global.Logger.WarnFormat("Client {0}({1})attempted to modify 0x7001!", session.ClientId,
                    session.ClientIp);
                return new PacketResult(PacketResultType.Disconnect);
            }
            
            session.CharnameSent = true;

            
            session.Charid = Task.Run(() =>
                    Program.DatabaseManager.connection.QueryAsync<int>(
                        $"SELECT CharID FROM {SettingsManager.GlobalSettings.Database.SharDb}.dbo._Char WHERE CharName16 = @charname",
                        new {charname = session.Charname}).Result
                    .First())
                .Result;

            return new PacketResult(PacketResultType.Nothing);
        }

        private async Task<PacketResult> RequestHwidPacket(Packet packet, Session session)
        {
            // block else dc from gameserver since the opcode (aka msgid) is unknown)
            session.Hwid = packet.ReadAscii();
            return new PacketResult(PacketResultType.Block);
        }

        private async Task<PacketResult> Hwid(Packet packet, Session session)
        {
            // block else dc from gameserver since the opcode (aka msgid) is unknown)
            session.Hwid = packet.ReadAscii();
            return new PacketResult(PacketResultType.Block);
        }
    }
}