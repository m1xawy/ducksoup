﻿namespace DuckSoup.Management
{
    public class ManagementSettings
    {
        public int Version;
        public string Host;
        public int Port;
        public int Retries;
        public bool AutoReconnect;
        
        public ManagementSettings Init()
        {
            Version = 1;
            Host = "0.0.0.0";
            Port = 3000;
            Retries = 5;
            AutoReconnect = true;
            
            return this;
        }
    }
}