﻿using System.Collections.Generic;
using DuckSoup.Library;

namespace DuckSoup.Download
{
    public class DownloadSettings : Settings
    {
        public int Version;
        
        public List<ushort> ClientWhitelist;
        public List<ushort> ClientBlacklist;

        public DownloadSettings Init()
        {
            Version = 1;
            
            ClientWhitelist = new List<ushort>
            {
                0x2002, // CLIENT_GLOBAL_MODULE_KEEP_ALIVE (Empty)
                0x6004 // CLIENT_DOWNLOAD_FILE_REQUEST 
            };
            
            ClientBlacklist = new List<ushort>();

            return this;
        }
    }
}