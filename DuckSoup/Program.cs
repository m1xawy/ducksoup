﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Timers;
using DuckSoup.Agent;
using DuckSoup.Download;
using DuckSoup.Gateway;
using DuckSoup.Library;
using DuckSoup.Library.Database;
using log4net;
using log4net.Config;
using Timer = System.Timers.Timer;

[assembly: XmlConfigurator(Watch = true)]

namespace DuckSoup
{
    public static class Program
    {
        private static Timer _consoleTitleTimer;
        private static bool _stopped;
        private static AsyncServer _asyncServer;
        private static SettingsManager _settingsManager;
        public static DatabaseManager DatabaseManager;
        private static ILog _logger = Global.Logger;


        private static void Main()
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));

            // prints out logo + version
            _logger.InfoFormat("\n\n" +
                               ",--.          .   .---.             \n" +
                               "|   \\ . . ,-. | , \\___  ,-. . . ,-. \n" +
                               "|   / | | |   |<      \\ | | | | | | \n" +
                               "^--'  `-^ `-' ' ` `---' `-' `-^ |-' \n" +
                               "                                |   \n" +
                               "         Version  {0}         ' \n" +
                               "\n",
                System.Diagnostics.FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location)
                    .ProductVersion);
            Console.Title = $"Starting up...";

            _settingsManager = new SettingsManager();
            if (_settingsManager.GlobalSettings.ServerType != ServerType.None)
                DatabaseManager = new DatabaseManager(_settingsManager);

            // initialize 
            switch (_settingsManager.GlobalSettings.ServerType)
            {
                case ServerType.GatewayServer:
                    _asyncServer = new GatewayServer(_settingsManager);
                    Task.Run(() => _asyncServer.Start());
                    break;
                case ServerType.DownloadServer:
                    _asyncServer = new DownloadServer(_settingsManager);
                    Task.Run(() => _asyncServer.Start());
                    break;
                case ServerType.AgentServer:
                    _asyncServer = new AgentServer(_settingsManager);
                    Task.Run(() => _asyncServer.Start());
                    break;
                case ServerType.ManagementServer:
                    break;
                case ServerType.None:
                    Console.Title = $"No server selected!";
                    goto CommandLoop;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (_stopped != true)
            {
                _logger.Info("Please enter help all commands!");
                Console.Title =
                    $"{_asyncServer.SettingsManager.GlobalSettings.ServerName} Connections: {_asyncServer.Sessions.Count} - Peak {_asyncServer.Counter}";
                _consoleTitleTimer = new Timer();
                _consoleTitleTimer.Elapsed += OnTimedEvent;
                _consoleTitleTimer.Interval = 1000;
                _consoleTitleTimer.Enabled = true;
            }

            CommandLoop:
            while (!_stopped)
            {
                var command = Console.ReadLine();
                var split = command?.Split(" ");
                switch (split?[0].ToLower())
                {
                    case "stop":
                    case "exit":
                        Stop();
                        break;
                    case "help":
                        _logger.Info("help");
                        _logger.Info("stop, exit");
                        break;
                    default:
                        _logger.Info("Please enter help to see all commands!");
                        break;
                }
            }
        }

        public static void Stop()
        {
            if (_consoleTitleTimer != null)
                _consoleTitleTimer.Enabled = false;
            _asyncServer?.Stop();
            _stopped = true;
        }

        private static void OnTimedEvent(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            Console.Title =
                $"{_asyncServer.SettingsManager.GlobalSettings.ServerName} Connections: {_asyncServer.Sessions.Count} - Peak {_asyncServer.Counter}";
        }
    }
}